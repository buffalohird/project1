Ansel Duff
Buffalo Hird
Project1
Computer Science 164

README.txt - Instructions on constructing our mobile web application.

What We've Done:

    * Constructed the entirety of our app
    * Created main "Map" view/controller where all of our user's interaction occurs
    * Created javascript files for facebook, map, and recents
    * Successfully geolocated the user
    * Succesfully fetch a Google Map surrounding the user's current location
    * Allow the user to search for nearby locations by a keywork and a radius (via Google Places)
        -> A 'category' criterion may be added to the search criteria, but this is 
           still being debated (category being something like "shop," "hospital," etc.)
    * "Print" the user's search resuts onto the map
        -> Different 'kinds' of results are denoted by different marker icons
    * Add recently clicked markers to localStorage for a list of recently viewed searches
        -> We're store the title of the marker and the user's search material
        -> When the user clicks on a recent item, a post call is made to the Facebook api
    * Add listenrs to the map. This allows the user to click on a marker and a text box pops up with some information
      regarding the marker.
        -> When the user clicks on the infowindow a post call is made to the Facebook api
    * Added Facebook API support with a jquery mobile dialog plugin to confirm posts made on our site
    

Known Issues (every project has 'em):
    * We assume the user is on our domain if they want to use login functionality.  There is no security concern here since we need facebook's response and there is little reason to support such in a production environment
    * We are warned that Google Maps is loaded multiple times, though to use places and maps there is no other way to load in the files
    * jQuery Mobile's stock slider (used for allowing the user to set a radius for their search) is currently experiencing
      compatibility errors. The slider doesn't work in Chrome, though it does work when we tested on iOS/Android Devices.
    * Warnings are sometimes thrown when navigating the map related to images, such seems to be a google compatability issue, but doesn't affect functionality
    * We chose to not use Facebook's location capabilites 1) because Facebook uses bing and we used google 2) the new Facebook API has little to no documentation and no relevant online examples that function
    * There are some CSS compatibility issues accross different browsers. We've tried to create a simple yet powerful user interface but we'd be lying if we didn't admit that sometimes we have experienced isses in the page's rendering.
    


Testing Instructions
___________________________________________________________

Optimally, visit http://164Square.com

If cloned to a localhost, recall that our Facebook authentication will no longer work. Facebook requires an authenticated URL in order to receive any feedback from applications. That is why we hosted our web-app at
the above domain name so that FB authenticaion can work (and it does!).

To View Code, etc.
------------------------------------------------------------
# clone repo into ~/vhosts/buffalohird
cd ~/vhosts
git clone git@bitbucket.org:buffalohird/project1.git buffalohird

# chmod all directories 711
find ~/vhosts/buffalohird -type d -exec chmod 711 {} \;

# chmod all PHP files 600
find ~/vhosts/buffalohird -type f -name *.php -exec chmod 600 {} \;

# chmod most everything else 644
find ~/vhosts/buffalohird -type f \( -name *.css -o -name *.gif -o -name *.html -o -name *.js -o -name *.jpg -o -name *.png -o -name .htaccess \) -exec chmod 644 {} \;

# ensure the default controller is set to "map" NOT "welcome"
gedit ~/vhosts/buffalohird/application/config/routes.php so that line 41 reads "$route['default_controller'] = "map";"

# append '127.0.0.1 buffalohird' to /etc/hosts
sudo gedit /etc/hosts
