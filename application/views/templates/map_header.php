<!-- *********************************************************
 *
 * header.php
 * 
 * Holds required files for each page
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->

<!DOCTYPE html>

<!-- loads in required .js and .css files -->
<html>
  <head>
  
    <meta name="viewport" content="width=device-width">
    <title><?php echo htmlspecialchars($title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" type="text/css"/>
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>
    <!-- <link rel="stylesheet" href="style.css" /> -->
    <!-- custom css file loaded from web to prevent errors -->
	<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
	
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCHUQKIosQIUMNuP569eOIXlSh4CZ3rINg&sensor=false"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
    <script type="text/javascript" src="/js/map.js"></script>
    <script type="text/javascript" src="/js/recents.js"></script>

  </head>
  
  <body onload="initialize();">
     
    <!-- allows for dynamic titling of pages using the header-->
    <div data-role="page" id="page">

    <div data-role="header" data-theme="b" >
          
          <h1><?php echo htmlspecialchars($title) ?></h1>
          
