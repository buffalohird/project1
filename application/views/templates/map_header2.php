<!-- *********************************************************
 *
 * header.php
 * 
 * Holds required files for each page
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->

<!DOCTYPE html>

<!-- loads in required .js and .css files -->
<html>
  <head>
  <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map { height: 500px; width: 1600px}
    </style>
  
    <meta name="viewport" content="width=device-width">
    <title><?php echo htmlspecialchars($title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />
    <!-- <link rel="stylesheet" href="style.css" /> -->
    <!-- custom css file loaded from web to prevent errors -->
	<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
	
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCHUQKIosQIUMNuP569eOIXlSh4CZ3rINg&sensor=false"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
    <script type="text/javascript">
        function initialize() 
        {
            // geolocate the user via HTML5's handy dandy geolocaiton tool
            navigator.geolocation.getCurrentPosition(map); 
            
            // display the map around the user
            function map (user)
            {
                /*var you_icon = new google.maps.MarkerImage('themes/you.png',
                // This marker is 20 pixels wide by 32 pixels tall.
                new google.maps.Size(20, 32));*/
                
                // set the user's location
                user_location = new google.maps.LatLng(user.coords.latitude, user.coords.longitude);
                
                // set our map params
                var myOptions = 
                {
                    // aside from center, these are somewhat arbitrary (for now!)
                    center: user_location,
                    zoom: 16, // just picked on what would be most convenient for user
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                          
                // inject our map into the page
                map = new google.maps.Map(document.getElementById("map"), myOptions);
                
                marker = new google.maps.Marker({
                    position: user_location,
                    map: map,
                    title:"You are here!"
                }); 
                
                // store the user's info in localstore (if not already done so) for the other parts of the site to access
                if(localStorage.getItem('user') == null )
                {
                    var local_user = {};
                        local_user.lat = user.coords.latitude;
                        local_user.lng = user.coords.longitude;
                        
                    localStorage.setItem('user', JSON.stringify(local_user));
                }
                
                user_radius = 1000;//$('#radius').val(); // in meters
                user_keyword = 'coffee';//$('#search').val();
                
                console.log('test');
                console.log(user_radius);
                
                var request = 
                {
                    location: user_location,
                    radius: user_radius,
                    keyword: user_keyword
                };
                
                service = new google.maps.places.PlacesService(map);
                service.search(request, callback);
      
                function callback(results, status) 
                {
                    if (status == google.maps.places.PlacesServiceStatus.OK) 
                    {
                        for (var i = 0; i < results.length; i++)
                        {                            
                            var search_marker = new google.maps.Marker({
                                position: results[i].geometry.location,
                                map: map,
                                title: results[i].name,
                                icon: results[i].icon
                            }); 
                        }
                    }
                }
           
        }    
    }
    </script>
  </head>
  
  <body onload="initialize();">
     
    <!-- allows for dynamic titling of pages using the header-->
    <div data-role="page" id="page">

    <div data-role="header" data-theme="b" >
          
          <h1><?php echo htmlspecialchars($title) ?></h1>
          
