<!-- *********************************************************
 *
 * header.php
 * 
 * Includes all necessary CSS, JS for the Map view
 * Also sets the stage for the rest of the view (i.e. title, closes head, opens body)
 *
 * Ansel Duff
 * Computer Science 164
 * Project1
 *
 *
 ********************************************************* -->

<!DOCTYPE html>

<!-- loads in required .js and .css files -->
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    
    <title><?php echo htmlspecialchars($title) ?></title>
    
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCHUQKIosQIUMNuP569eOIXlSh4CZ3rINg&sensor=false"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
    	
    <link rel="stylesheet" href="/css/jq.css" type="text/css"/>
    <link rel="stylesheet" href="/css/jqm.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.css" /> 
    <link rel="stylesheet" href="/css/style.css" type="text/css"/>
    
	<script type="text/javascript" src="/js/jq.js"></script>
	<script type="text/javascript" src="/js/jqm.js"></script>
    <script type="text/javascript" src="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog2.min.js"></script>
    <script type="text/javascript" src="/js/facebook.js"></script> 
    <script type="text/javascript" src="/js/map.js"></script>
    <script type="text/javascript" src="/js/recents.js"></script>


  </head>
  
  <body onload="initialize();" id="map_body" >
     
    <!-- allows for dynamic titling of pages using the header-->
    <div data-role="page" id="page">

    <div id="main_header" data-role="header" data-theme="b">
          
          <h1><?php echo htmlspecialchars($title) ?></h1>
          
