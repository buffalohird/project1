<!-- *********************************************************
 *
 * map/index.php
 * 
 * View for Homepage (map, search field)
 *
 * Ansel Duff
 * Computer Science 164
 * Project1
 *
 *
 ********************************************************* -->
        <!-- buttons for navigation to the search menu and the shopping list -->
        <!-- <a href="" data-icon="refresh" data-transition="slide" data-direction="reverse" >Recently Viewed</a> -->
        
    <div data-role="navbar">
		<ul>
            <li><a id="search_button" data-role="button">Search!</a></li>
            <li><a id="recents_button" data-role="button">Recently Viewed</a></li>
            <li><button id="log_in_button" data-role="button" value="Log In" data-corners="false"></button></li>
		</ul>
	</div><!-- /navbar -->
    
        <div id="fb-root"></div>
    </div><!-- /header --> 
    
    <div>
        <ul data-role="listview" data-theme="b" id="recents_list">
        </ul>
    </div>
    
    <div data-role="content" id="map_container">  
    <div id= "map">
    </div> <!-- /map -->
    
</div><!-- /page -->
    
<div id="form_container">
    <form>
        <input type="text" name="keyword" id="search" placeholder="search"/>               
        <label for="radius">Search Radius in km:</label>
        <input type="range" name="radius" id="radius" value="2" min="0" max="5000"/>
    </form>
</div>
