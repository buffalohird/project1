<?php
	
	/***********************************************************
	 *
	 * map.php
	 * 
	 * Controller for welcome page showing the user's map.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project1
	 *
	 *
	 ************************************************************/
	 
	// again, carry the class
	class Search extends CI_Controller
	{
		//$this->load->helper('url');
		
		public function index()
		{			
			$send['title'] = "164Square | Search";
			
			// load view
			$this->load->view('templates/header', $send);
			$this->load->view('search/index', $send);
			$this->load->view('templates/footer');
			
			echo "success";
		}
	}
?>
