<?php
	
	/***********************************************************
	 *
	 * welcome.php
	 * 
	 * Controller for welcome page.
	 *
	 * Buffalo Hird
	 * Computer Science 164
	 * Project1
	 *
	 *
	 ************************************************************/
	 
	// again, carry the class
	class Welcome extends CI_Controller
	{
		public function index()
		{
			// load view
			$this->load->view('templates/header', array('title' => 'Home'));
			$this->load->view('welcome/index');
			$this->load->view('templates/footer');
		}
	}
?>
