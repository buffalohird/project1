<?php
	
	/***********************************************************
	 *
	 * map.php
	 * 
	 * Controller for welcome page showing the user's map.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project1
	 *
	 *
	 ************************************************************/
	 
	// extend the stock controller class and make our Map controller
	class Map extends CI_Controller
	{
		// construct from parent, load the model
		public function __construct()
		{
		    parent::__construct();
    	}
		
		// used to create the page
		public function index()
		{		
			// our title
			$send['title'] = "164Square | Home";
			
			// load view(s)
			$this->load->view('templates/header', $send);
			$this->load->view('map/index');
			$this->load->view('templates/footer');
		}
	}
?>
