/*****************************************************************************
 *
 * html/js/map.js
 *
 * JS file whichs creates/ supplies features to the map
 *
 * Ansel Duff
 * Computer Science 164
 * Project1
 *
 *
*****************************************************************************/
    
    function initialize() 
    {       
        // to keep track of if the seach form is displayed or not. when the page loads, the form is up
        var form = "on";
        var recents = "off";
        
        // so that we can show the user if they've already checked in
        check_arr = new Array();
        
        // every time the user visits (loads) the page, their recently viewed locations will be reset
        localStorage.clear();
        
        // don't clog map space with the recents list when the user visits the page
        $("#recents_list").hide('fast');
        
        // this array will allow us to clear the map of old searches
        markersArray = new Array();
        
        // grab the next available ID from storage (used to store recents in localStorage)
        var lastID = localStorage.getItem('lastID');
        
        // geolocate the user via HTML5's handy dandy geolocaiton tool
        navigator.geolocation.getCurrentPosition(map); 
        
        // the user clicks on the 'recently viewed' button
        $("#recents_button").click(function () {
            
            // if the list is hidden
            if (recents == "off")
            {
                // show our recent list, close the search form
                show_recents();
                $("#form_container").slideUp('slow');
                recents = "on";
                form = "off";
            }
            
            // hide the list
            else
            {
                $("#recents_list").slideUp('slow');
                recents = "off";
            }
        });
        
        // if the form is hidden, display it when the user clicks the search button
        $("#search_button").click(function () {
            if(form == "off")
            {
                $("#form_container").slideDown('slow');
                form = "on";
            }
            else
            {
                $("#form_container").slideUp('slow');
                form = "off";
            }
        });
        
        // if the user clicks the map, hide the search form/recents list
        $("#map").click(function ()
        {
            hide_form();   
        });
        
        // display the map around the user
        function map (user)
        {
            // made it myself O;
            var youser_image = new google.maps.MarkerImage('/themes/you_sm.png');
            
            // set the user's location
            user_location = new google.maps.LatLng(user.coords.latitude, user.coords.longitude);
            
            // set our map params
            var myOptions = 
            {
                // easiest to use
                center: user_location,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            // initialize our infoWindow's parameters
            infowindow = new google.maps.InfoWindow(
            {
                maxWidth: 200
            });
                    
            // inject our map into the page
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            
            // this is the "YOU" marker
            marker = new google.maps.Marker(
            {
                position: user_location,
                map: map,
                title:"You are here!",
                icon: youser_image
            }); 
            
       }
        
        // clears map using our array which remebers all the markers from the last search
        function clear_prev_search()
        {
            if (markersArray) 
            {
                for (var i = 0; i < markersArray.length; i++ ) 
                {
                    markersArray[i].setMap(null);
                }
            }
        }
        
        // used to hide the search form for more viewing room on the map
        function hide_form()
        {
            $("#form_container").slideUp('slow');
            $("#recents_list").slideUp('slow');
            form = "off";
            recents = "off";
        }
        
        function update_map()
        {
            clear_prev_search();
                   
            // we ask for a radius in km, so we multiply by 100 because Google only accepts radii in meters
            user_radius = ($('#radius').val() * 1000);
            user_keyword = $('#search').val();
                    
            // to be sent to Google Places
            var request = 
            {
                location: user_location,
                radius: user_radius,
                keyword: user_keyword
            };
                    
            // the actual call to Places
            service = new google.maps.places.PlacesService(map);
            service.search(request, callback);
          
            function callback(results, status) 
            {
                if (status == google.maps.places.PlacesServiceStatus.OK) 
                {
                    // iterate through our results and print each one as a marker on the map
                    for (var i = 0; i < results.length; i++)
                    {                            
                        var search_marker = new google.maps.Marker({
                        position: results[i].geometry.location,
                        map: map,
                        title: results[i].name,
                        icon: results[i].icon
                        }); 
                                        
                        // store each marker in our array to make clearing for the next search easy
                        markersArray[i] = search_marker;
                        
                        // ad the event listeners to the map (clicking on the markers)
                        listen(search_marker);
                    }
                }
            } 
        }

        
        // allow the user to be able to check in at a location
        function listen(current_marker)
        {   
            // these are the listeners that allow for the infoWindow
            google.maps.event.addListener(current_marker, 'click', function()
            {   
                // if the user has already checked in somewhere, don't allow him to check in twice
                if(search_check(current_marker.title))
                {
                    checked_filler = "You've already checked in at " + current_marker.title + "!";
                    filler = document.createElement("div");
                    filler.innerText= checked_filler;
                    infowindow.setContent(filler);
                    infowindow.open(map, current_marker);
                    
                    return false;
                }
                
                // general infoWindow filler text
                var text_filler = "Click here to check in at " + current_marker.title + ".";
                
                // the template for each infoWindow
                filler = document.createElement("div");
                filler.innerText= text_filler;
                $(filler).click(function()
                {
                    hide_form();
                    
                    // update the user's status and the map
                    facebook_check_in(current_marker.title, 'map');
                    update_map();
    
                });
                
                // more viewing room
                hide_form();
                
                // create each infoWindow
                infowindow.setContent(filler);
                infowindow.open(map, current_marker);
                
                // store recent searched in localStorage
                var recent_search = {};
                
                // set our parameters
                recent_search.title = current_marker.title;
                recent_search.key = user_keyword;
                recent_search.radius = user_radius;
                
                // grab the next available ID from storage
                var lastID = localStorage.getItem('lastID');
 
                // if the local storage comes up empty, reset count to zero
                if(lastID == null) 
                {
                    localStorage.setItem('lastID', 0);
                    var lastID = 0;
                }
        
                // create a ID number for the search 
                var recentID = lastID;
            
                // list starts at 0
                ID = 0;
                
                // gets set to false if our place has already been recently searched
                var already_recent = true;
                
                // iterates through list of stored recently viewed searches (of length LastID) to check if we've already viewed a location
                while(ID < lastID)
                {
                    // convert id to string to match localStorage
                    ID = ID.toString();
                
                    var checkRecent = localStorage.getItem(ID);
                    
                    // prompts if the attempted addition matches a search already on the list
                    if(checkRecent == JSON.stringify(recent_search))
                    {
                        // creates a bool conditional which will not allow the location to be added to the shopping list
                        already_recent = false;  
                        
                        // our duplicate is found, no need to continue iterating
                        break;
                    }
                    
                    // on to the next item in localStorage
                    ID++;
                } 
             
                // occurs if there is no error flagged by the identical location check
                if(already_recent == true)
                {   
                    // turn location data into JSON string for storage
                    localStorage.setItem(recentID, JSON.stringify(recent_search));
            
                    // increments the location ID and stores it locally
                    recentID++;
                    
                    // item removed then added to avoid compatibility errors
                    localStorage.removeItem('lastID');
                    localStorage.setItem('lastID', recentID);
                }
            });
        }
        


        
        // user hits enter on the text search field
        $("#search").bind("keypress", function(e) 
        {
            var c = e.which ? e.which : e.keyCode;
            if (c == 13) 
            {
                hide_form();
                update_map();
            
                // prevents HTML from activating get request
                e.preventDefault();
                return false;
            } 
        });
        
        // user changes the search radius (this requires them to hit enter after changing the value, simply draggind the slider or clicking the up/down arrows will do nothing)
        $("#radius").change(function()
        {
            update_map();
        });
      
    } // end of initialize()
