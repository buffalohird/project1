/* *********************************************************
 *
 * facebook.js
 * 
 * Javascript for Facebook functionality
 *
 * Buffalo Hird
 * Computer Science 164
 * Project1
 *
 *
 ********************************************************* */
      
        //facebook authenticates itself
        window.fbAsyncInit = function() {
          FB.init({
            appId      : '312649065459153',
            status     : true, 
            cookie     : true,
            xfbml      : true,
            oauth      : true,
          });
          
          //checks the facebook login status.  sets the login/logout button based on this status
          FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                $("#log_in_button").prev('.ui-btn-inner').children('.ui-btn-text').html('Log Out');          
            } 
            else 
            {
                $("#log_in_button").prev('.ui-btn-inner').children('.ui-btn-text').html('Log In');   
            }
        });
          
        };
        
        //facebook initializes itself
        (function(d){
           var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
           js = d.createElement('script'); js.id = id; js.async = true;
           js.src = "//connect.facebook.net/en_US/all.js";
           d.getElementsByTagName('head')[0].appendChild(js);
         }(document));
         
        //a dual function used to log the user in or out
        function facebook_log() {
            FB.getLoginStatus(function(response) {
            //if there is a response of connected, the user is logged out and the log button changes to 'login'
            if (response.status === 'connected') {
                FB.logout(function(response) {
                    $("#log_in_button").prev('.ui-btn-inner').children('.ui-btn-text').html('Log In'); 
                });          
            }
            //if not connected, the user is prompted for log in and the log button changes to 'logout' if succesful 
            else 
            {
                FB.login(function(response) {
                if (response.authResponse) {
                    $("#log_in_button").prev('.ui-btn-inner').children('.ui-btn-text').html('Log Out'); 
                }                
                // we need the assumed permissions, email, and the ability to publish as the user as permissions from facebook              
                }, {scope: 'email,publish_actions' });
            }
           
            });
        }
        
        //pop up dialog which prompts for log in
        function facebook_popup() {
            $('<div>').simpledialog2({
                mode: 'button',
                headerText: 'Error',
                headerClose: true,
                buttonPrompt: 'You Are Not Logged In!',
                buttons : {
                    'Log In': {
                        click: function () {
                            facebook_log();
                        }
                    },
                    'Cancel': {
                        click: function () { 
                            //nothing
                        },
                    icon: "delete",
                    theme: "c"
                    }
                }
            })
        }       
         
        //check in function     
        function facebook_check_in(title, source, item) {
            
            check_in_boolean = false;
            
            //we ping facebook and if the returned user has a name we assume we can proceed
            FB.api('/me', function(response) {
            if(response.name != undefined) {
            
                //creates a boolean that is used to tell the map/recent javascript check in was succesful

            
                //we create a newPost object to store the facebook post information
                newPost = {};
                    newPost['message'] ='Checking in at ' + title;
                    newPost['name'] = '164Square';
                    newPost['description'] = 'I just checked in with 164square!';
                    newPost['link'] = 'http://164square.com';
                    newPost['picture'] = 'http://www.mediafire.com/imgbnc.php/4867cff9420348225ec4bb99b364536ef451be3e4d759b43bae36810952fdab25g.jpg';
                    newPost['caption'] = 'I Was Here!';

                
                post_popup(title, source, item);
                           
            }
            
            //if the user was not logged in, we send the facebook login popup, which prompts the user to login for check in functionality
            else {
                facebook_popup();
            }
            
            });
            
        } 
        
        //pop up dialog which prompts for check in confirmation
        function post_popup(title, source, item) {
            $('<div>').simpledialog2({
                mode: 'button',
                headerText: 'Check In?',
                headerClose: true,
                buttonPrompt: 'Do you want to check in at ' + title + ' on Facebook?',
                buttons : {
                    'Update Status!': {
                        click: function () {
                        
                            //we post the newPost object to the user's facebook feed            
                            FB.api('/me/feed', 'post', newPost , function(response) {
                            if (response.error) {
                            
                                // fades in the error text for recents
                                if(source == 'recents') { 
                                    item.fadeOut('fast', function ()
                                    {
                                        item.html('<h3>Could not check you in at ' + title + ' =[</h3>');
                                        item.fadeIn('fast'); 
                                    });
                                }
                                 // fades in the error text for map
                                 else if(source == 'map') {
                                    $(filler).text("Could not check you in at " + title + " =[");
                                }
                            }
                            else {
                    
                                // we've checked in here so remember it when the user looks as the recently viewed locations again
                                check_arr.push(title);
                                
                                // fades in the checked in text for recents
                                if(source == 'recents') { 
                                    item.fadeOut('fast', function ()
                                    {
                                        item.html('<h3>You\'ve checked in!</h3>');
                                        item.fadeIn('fast'); 
                                    });
                                }
                                // fades in the checked in text for the map
                                else if(source == 'map') {
                                    $(filler).fadeOut('fast', function ()
                                    {
                                        $(this).text("You've checked in!");
                                        $(this).fadeIn('fast'); 
                                    });
                                }
                            }
                            });
                        
                        }
                    },
                }
            })
        }
        
        //pop up dialog which prompts for log in
        function facebook_popup() {
            $('<div>').simpledialog2({
                mode: 'button',
                headerText: 'Error',
                headerClose: true,
                buttonPrompt: 'You Are Not Logged In!',
                buttons : {
                    'Log In': {
                        click: function () {
                            facebook_log();
                        }
                    },
                    'Cancel': {
                        click: function () { 
                            //nothing
                        },
                    icon: "delete",
                    theme: "c"
                    }
                }
            })
        } 
         
        //when the log button is clicked, we call the login/logout function to handle this user action
        $(function() {
            $("#log_in_button").click(function(){
                facebook_log();
            });
        });
 
