/*****************************************************************************
 *
 * html/js/recents.js
 *
 * JS file whichs allows user to recently viewed location searches
 *
 * Ansel Duff
 * Computer Science 164
 * Project1
 *
 *
*****************************************************************************/
    
    
    function clear_list()
    {
        $("#recents_list").empty()
    }
    
    // search through the list of places into which we've already checked in
    function search_check (title)
    {
        for(var i = 0; i <= check_arr.length; i++)
        {
            if(title == check_arr[i])
            {
                return true;
            }
        }
        
        return false;
    }
    
    function show_recents()
    {
        // the number of items in localStorage
        var length = localStorage.length;
        
        clear_list();
        
        // loads number of locations from storage so that each can be added to recents list
        var lastID = localStorage.getItem('lastID');
        
        // we're going to iterate through the 3 most recently viewed searches
        if (lastID >= 3)
            ID = (lastID - 3);
        // acount for condition where user hasn't viewed 3 locations yet
        else
            ID = 0;
        
        // no recents
        if(length == 0)
        {
            $("#recents_list").append($("<li></li>").html(
                '<h3><b><i>You have no recently viewed locations.</i></b></h4>'
            )); 
        }
        
        // we want the most recently viewed to be at the top of the list
        else
            var counter = lastID;
        
        // corner case so it will not iterate through an empty shopping list
        while(length != 0  && ID < counter)
        {                
            // decrement our counter first to attain proper number of recents
            counter--;
            
            // convert id to string to match localStorage
            counter.toString();

            // get the localStorage data
            var newRecent = localStorage.getItem(counter);
             
            // retrieves location object via stored JSON string
            var recent = $.parseJSON(newRecent);
                           
            // corner case so it skips a removed location in localstorage
            if(recent == null)
                continue;
            
            // if the user has already checked in, let him/her know when they review the recent list
            if(search_check(recent.title))
            {                    
                $("#recents_list").append($('<li name = ' + recent.title.split(' ').join('_') + ' class="recents" ></li>').html
                (
                    '<h3>You\'ve already Checked In at the ' + recent.title + '</h3>'
                ));
                
                continue;
            }
            
            // what the <li> will have inside of it
            var list_content =  '<h3><b>Click here to Check In at the ' + recent.title + '</b></h3>' +
                                '<p>You searched using the keyword \"' + recent.key + '\" within a ' + (recent.radius / 1000) + ' km radius.</p>';
            
            // the actual creation of the dynamic list
            $("#recents_list").append($('<li name = ' + recent.title.split(' ').join('_') + ' class="recents" ></li>').html
            (
                list_content
            ));
        }
        
        // click on one item in the list
        $(".recents").click(function()
        {            
            // get the data specific to each list item
            var item = $(this);   
            var marker_title = $(this).attr('name');
            marker_title = marker_title ? marker_title.split('_').join(' ') : '';

            // the user has already checked in or there are no items in the list. for both cases, do nothing
            if(search_check(marker_title) || length == 0)
            {
                return false;
            }
            
            // check in and update the map
            facebook_check_in(marker_title, 'recents', item);
            infowindow.close();
        });
        
        // refresh the list, then animate
        $('#recents_list').listview('refresh');
        $("#recents_list").slideDown('slow');
    }
